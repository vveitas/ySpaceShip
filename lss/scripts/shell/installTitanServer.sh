#!/bin/sh

#installDir="$1"
#cd $installDir
if [ ! -d titan-server-0.4.4 ]; then
  if [ ! -f titan-server-0.4.4.zip ]; then
    wget http://s3.thinkaurelius.com/downloads/titan/titan-server-0.4.4.zip
  fi
  unzip titan-server-0.4.4.zip
  rm -r titan-server-0.4.4.zip
  cd titan-server-0.4.4/conf
  rm cassandra.yaml
  rm rexster-cassandra-es.xml
  ln -s ../../configs/cassandra.yaml cassandra.yaml
  ln -s ../../configs/rexster-cassandra-es.xml rexster-cassandra-es.xml
else
  echo "Titan server already exists in the install directory - not installing"
fi
