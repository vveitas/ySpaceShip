#!/bin/bash


if [ $1 == 'start' ]; then
    ../required/elasticsearch-0.90.3/bin/elasticsearch
else
    if [ $1 == 'stop' ]; then
      # Shutdown local node
      curl -XPOST 'http://localhost:9200/_cluster/nodes/_local/_shutdown'
      if [ $# -eq 2 ] && [] $2 == 'cluster' ]; then
        # Shutdown all nodes in the cluster
        curl -XPOST 'http://localhost:9200/_shutdown'
      fi
    else
        if [ $1 == 'status' ]; then
            ps aux | grep elasticsearch
        fi
    fi
fi
