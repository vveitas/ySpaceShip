@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.transform.TypeChecked
import groovy.lang.GroovyClassLoader

import ySpaceShip
import ConstructResultJson

import groovy.json.JsonSlurper
import groovy.json.JsonOutput

import org.apache.commons.io.FilenameUtils

import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.blueprints.Edge

import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import org.apache.commons.configuration.BaseConfiguration
import org.apache.commons.configuration.Configuration
import org.apache.log4j.*;

import static org.junit.Assert.assertNotNull

import com.thinkaurelius.titan.core.TitanFactory
import com.thinkaurelius.titan.core.TitanGraph
import com.tinkerpop.blueprints.impls.tg.TinkerGraph
import com.tinkerpop.blueprints.util.ElementHelper
import com.tinkerpop.pipes.PipeFunction
import com.tinkerpop.gremlin.java.GremlinPipeline

class EnableFilters {

  static TitanGraph g = ySpaceShip.g
  static Logger logger
  static config = new JsonSlurper().parse(new File(System.getProperty("user.dir") + "/configs/pbrain.conf"))
  static ArrayList subgraph
  static boolean takeIntoAccountSG

  @TypeChecked
  public static void run(Map data) {
    long start = System.currentTimeMillis()

    // setting up logging stuff
    def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
    PropertyConfigurator.configure(config.toProperties())
    this.logger = LoggerFactory.getLogger('enableFilters.class');

    //def parameters = new JsonSlurper().parseText(data)
    def parameters = data;

    calculateSubgraph(parameters);
    long finish = System.currentTimeMillis()
    logger.warn("Main method finished in {} seconds",(finish-start)/1000)
  }

  @TypeChecked
  static Map calculateSubgraph(Object parametersObject) {
    long start = System.currentTimeMillis()

    Map parameters = (Map) parametersObject
    def tags = Eval.me((String) parameters.checkedTags);
    def authors = Eval.me((String) parameters.checkedAuthors);
    def publications = Eval.me((String) parameters.checkedPublications);
    def files = Eval.me((String) parameters.checkedFiles);
    def temp = Eval.me((String) parameters.fullTextQuery)
    def fullTextQuery = ((ArrayList) Eval.me((String) parameters.fullTextQuery)).join(" ");
    def fullTextSearchFields = Eval.me((String) parameters.fullTextSearchFields);

    subgraph = []
    // take into account only something exists in subgraph, othrewise cosider everything
    // this is needed for the full text search
    takeIntoAccountSG = false;

    tags.each { tag ->
        takeIntoAccountSG = true;
        subgraphFromTags((String) tag);
    }
    authors.each { author ->
        takeIntoAccountSG = true;
        subgraphFromAuthors((String) author);
    }

    publications.each { publication ->
        takeIntoAccountSG = true;
        subgraphFromPublications((String) publication);
    }

    files.each { file ->
        takeIntoAccountSG = true;
        subgraphFromFiles((String) file);
    }

    def matchingVertexes = new HashSet();
    if (fullTextQuery != "") {
      matchingVertexes = fullTextSearch((String) fullTextQuery, (ArrayList<String>) fullTextSearchFields)
      logger.warn("Full text search found matchingVertexes {}", matchingVertexes)
    }
    else {
      matchingVertexes = subgraph.toSet();
      logger.warn("Full text search is disabled, returning subgraph {}", matchingVertexes);
    }

    def result = ConstructResultJson.constructResultJson((HashSet) matchingVertexes);
    def wholeGraphFile = System.getProperty("user.dir")+"/resources/enableFiltersTemp.dat"
    //new File(wholeGraphFile).write("");
    new File(wholeGraphFile).write(result.toString())

    //print result
    long finish = System.currentTimeMillis()
    logger.warn("calculateSubgraph method finished in {} seconds",(finish-start)/1000)

}
  @TypeChecked
  static void subgraphFromTags(String tag) {
    long start = System.currentTimeMillis()

    this.logger.warn("Subgraphing tagVertex {}", tag.toString());
    Vertex tagVertex = g.getVertex(tag)
    GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(tagVertex).store(subgraph).inE('isTagged').outV().store(subgraph).outE('isAuthored').inV().store(subgraph).iterate();
    pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(tagVertex).outE('pdfAttachment').inV().outE('isAnnotated').inV().store(subgraph).iterate();
    this.logger.warn("Added tag {} traversal to subgraph {} resulting in {} vertices", tag, subgraph.toString(), subgraph.size());
    
    long finish = System.currentTimeMillis()
    logger.warn("subgraphFromTags method finished in {} seconds",(finish-start)/1000)
  }

  @TypeChecked
  static void subgraphFromAuthors(String author) {
    long start = System.currentTimeMillis()
    this.logger.warn("Subgraphing authorVertex {}", author);

    Vertex authorVertex = g.getVertex(author)
    GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(authorVertex).store(subgraph).inE('isAuthored').outV().store(subgraph).outE('isTagged').inV().store(subgraph).iterate();
    pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(authorVertex).outE('pdfAttachment').inV().outE('isAnnotated').inV().store(subgraph).iterate();
    this.logger.warn("Added author {} traversal to subgraph {} resulting in {} vertices",author, subgraph.toString(),subgraph.size());
    
    long finish = System.currentTimeMillis()
    logger.warn("subgraphFromAuthors method finished in {} seconds",(finish-start)/1000)
  }

  @TypeChecked
  static void subgraphFromPublications(String publication) {
    long start = System.currentTimeMillis()
    this.logger.warn("Subgraphing publicationVertex {}", publication);

    Vertex publicationVertex = g.getVertex(publication)
    GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(publicationVertex).store(subgraph).outE('isAuthored').inV().store(subgraph).iterate();
    pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(publicationVertex).outE('isTagged').inV().store(subgraph).iterate();
    pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(publicationVertex).outE('pdfAttachment').inV().outE('isAnnotated').inV().store(subgraph).iterate();
    this.logger.warn("Added publication {} traversal to subgraph {} resulting in {} vertices",publication, subgraph.toString(),subgraph.size());
    
    long finish = System.currentTimeMillis()
    logger.warn("subgraphFromPublications method finished in {} seconds",(finish-start)/1000)
  }

  @TypeChecked
  static void subgraphFromFiles(String file) {
    long start = System.currentTimeMillis()
    this.logger.warn("Subgraphing fileVertex {}", file);

    Vertex fileVertex = g.getVertex(file)
    GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
    pipe.start(fileVertex).store(subgraph).outE('isAnnotated').inV().store(subgraph).iterate();
    this.logger.warn("Added file {} traversal to subgraph {} resulting in {} vertices",file, subgraph.toString(),subgraph.size());
    long finish = System.currentTimeMillis()
    logger.warn("subgraphFromFiles method finished in {} seconds",(finish-start)/1000)
  }

  @TypeChecked
  static HashSet fullTextSearch(String queryString, ArrayList<String> searchFields) {
    def verticesSet = new HashSet()
    searchFields.each { field ->
      String query = "v.${field}:(${queryString})"
      logger.warn("Executing a query string {}", query);
      def result = g.indexQuery("search",query).vertices()
      logger.warn("Got result {}", result)
      result.each {
        def element = it.getElement();
        if (element != null) {
            if (!takeIntoAccountSG || subgraph.contains(element) !=null) {
              verticesSet.add(element)
              logger.warn("Retrieved vertex {}",element.toString())
            } else {
              logger.warn("Vertex {} does not exist in subgraph, therefore not adding",element.toString())
            }
        }
      }
    }
    logger.warn("Elements matching full text query: {}",verticesSet)
    return verticesSet
  }

}
