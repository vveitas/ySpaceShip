@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')

import groovy.lang.GroovyClassLoader
import groovy.transform.TypeChecked
import groovy.json.JsonSlurper
import groovy.json.JsonOutput

import SpaceGraph
import EnableFilters

import com.thinkaurelius.titan.core.TitanGraph

import java.net.ServerSocket

public class ySpaceShip {

    static SpaceGraph sg = new SpaceGraph();
    public static TitanGraph g = sg.g;

    @TypeChecked
    public void ass() {
        Runtime.getRuntime().addShutdownHook(new Thread() {
            public void run() {
              sg.gracefulShutdown()
            }
          });
    }

    @TypeChecked
    public static void main(String[] args) {
        def server = new ServerSocket(3003)
        while(true) {
            server.accept { socket ->
                socket.withStreams { input, output ->
                    def reader = input.newReader()
                    def buffer = reader.readLine()
                    Map message = (Map) new JsonSlurper().parseText(buffer);
                    def classToRun = message.class;
                    def data = message.data;
                    switch (classToRun) {
                        case "EnableFilters":
                            new EnableFilters().run((Map) data);
                            break;
                    }
                }
            }
        }

    }


}