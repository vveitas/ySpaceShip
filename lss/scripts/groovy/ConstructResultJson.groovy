@Grab(group='com.thinkaurelius.titan', module='titan-es', version='0.4.4')
@Grab(group='com.thinkaurelius.titan', module='titan-cassandra', version='0.4.4')
@Grab(group='log4j', module='log4j', version='1.2.17')
@Grab(group='com.tinkerpop.gremlin', module='gremlin-java', version='2.4.0')

import groovy.transform.TypeChecked
import groovy.lang.GroovyClassLoader
import groovy.json.JsonOutput
import groovy.util.logging.Log4j
import org.apache.log4j.PropertyConfigurator
import org.slf4j.Logger
import org.slf4j.LoggerFactory
import com.tinkerpop.gremlin.groovy.Gremlin
import com.thinkaurelius.titan.core.TitanVertex
import com.tinkerpop.blueprints.Direction
import com.tinkerpop.blueprints.Edge
import com.tinkerpop.blueprints.Vertex
import com.tinkerpop.gremlin.java.GremlinPipeline

class ConstructResultJson {
      static Logger logger

      @TypeChecked
      static String constructResultJson(HashSet<Vertex> matchingVertexes) {
        long start = System.currentTimeMillis()
        
        // setting up logging stuff
        def config = new ConfigSlurper().parse(new File('configs/log4j-testing.groovy').toURL())
        PropertyConfigurator.configure(config.toProperties())
        this.logger = LoggerFactory.getLogger('ConstructResultJson.class');
        // continue constructing JSON object according to this:
        // http://docs.oracle.com/javaee/7/api/javax/json/JsonObjectBuilder.html

        def json = [ matchingAuthors: []
                  , matchingPublications: []
                  , matchingTags: []
                  , matchingPDFfiles: []
                  , matchingAnnotations: []
                  ]
        matchingVertexes.each{ mVertex ->
            logger.warn("The matchingVertex is of type {}", mVertex.getProperty("entry"))
            GremlinPipeline pipe = new GremlinPipeline<Vertex,Vertex>();
            int numberOfAnnotations = (int) pipe.start(mVertex).outE('pdfAttachment').inV().outE('isAnnotated').count()
            logger.warn("got numberOfAnnotations {}", numberOfAnnotations);

            switch (mVertex.getProperty("entry")) {
                case "person":
                    json.matchingAuthors.add([
                        id: mVertex.getId(),
                        firstName: mVertex.getProperty("firstName"),
                        lastName: mVertex.getProperty("lastName")
                    ])
                    break;
                case "publication":

                    pipe = new GremlinPipeline<Vertex,Vertex>();
                    List fileVertex = pipe.start(mVertex).outE('pdfAttachment').inV().toList();
                    logger.warn("got fileVertex list {}", fileVertex);

                    json.matchingPublications.add([
                        id: mVertex.getId(),
                        date: mVertex.getProperty("date"),
                        itemType: mVertex.getProperty("itemType"),
                        title: mVertex.getProperty("title"),
                        authorstext: mVertex.getProperty("authorstext"),
                        bibtexKey: mVertex.getProperty("bibtex_key"),
                        key: mVertex.getProperty("key"),
                        tagVertexIds: mVertex.getProperty("tagVertexIds"),
                        dateAdded: mVertex.getProperty("dateAdded"),
                        dateModified: mVertex.getProperty("dateModified"),
                        publisher: mVertex.getProperty("publisher"),
                        fileAttached: ! fileVertex.isEmpty(),
                        file: mVertex.getProperty("file"),
                        relativeFilePath:  fileVertex[0] ? fileVertex[0].getProperty("relativeFilePath") : "NoAttachment",
                        numberOfAnnotations: numberOfAnnotations
                    ])
                    break;
                case "tag":
                    json.matchingTags.add([
                        id: mVertex.getId(),
                        tag: mVertex.getProperty("tag")
                    ])
                    break;
                case "PDFfile":
                    pipe = new GremlinPipeline<Vertex,Vertex>();
                    List attachedPublications = pipe.start(mVertex).inE('pdfAttachment').toList()
                    logger.warn("Got attachedPublications list {}", attachedPublications);

                    json.matchingPDFfiles.add([
                        id: mVertex.getId(),
                        fileName: mVertex.getProperty("fileName"),
                        relativeFilePath: mVertex.getProperty("relativeFilePath"),
                        fileNameHash: mVertex.getProperty("fileNameHash"),
                        publicationAttached: ! attachedPublications.isEmpty(),
                        numberOfAnnotations: numberOfAnnotations
                    ])
                    break;
                case "pdfAnnotation":
                    pipe = new GremlinPipeline<Vertex,Vertex>();
                    def file = pipe.start(mVertex).inE('isAnnotated').outV().next(1)[0].getProperty("relativeFilePath");
                    logger.warn("got file relativePath {}", file);

                    pipe = new GremlinPipeline<Vertex,Vertex>();
                    def publication;
                    def interimPipe = pipe.start(mVertex).inE('isAnnotated').outV().inE('pdfAttachment').outV()
                    if (interimPipe.hasNext()) {
                        publication = interimPipe.next(1)[0].getProperty("title");

                    } else {
                        pipe = new GremlinPipeline<Vertex,Vertex>();
                        publication = pipe.start(mVertex).inE('isAnnotated').outV().next(1)[0].getProperty("fileName");
                    }

                    logger.warn("got publication title {}", publication);

                    pipe = new GremlinPipeline<Vertex,Vertex>();
                    def bibtexKey;
                    interimPipe = pipe.start(mVertex).inE('isAnnotated').outV().inE('pdfAttachment').outV()
                    if (interimPipe.hasNext()) {
                        publication = interimPipe.next(1)[0].getProperty("bibtex_key");
                    }

                    logger.warn("got bibtexKey {}", bibtexKey);
                    switch (mVertex.getProperty("type")) {
                      case "text":
                          json.matchingAnnotations.add([
                              id: mVertex.getId(),
                              author: mVertex.getProperty("author"),
                              uniqueName: mVertex.getProperty("uniqueName"),
                              displayText: mVertex.getProperty("popupContents"),
                              type: mVertex.getProperty("type"),
                              subType: mVertex.getProperty("subType"),
                              date: mVertex.getProperty("modificationDate"),
                              dateAdded: mVertex.getProperty("creationDate"),
                              publication: publication,
                              relativeFilePath: file,
                              page: mVertex.getProperty("page"),
                              bibtexKey: bibtexKey
                          ])
                          break;
                      case "highlight":
                          json.matchingAnnotations.add([
                              id: mVertex.getId(),
                              author: mVertex.getProperty("author"),
                              uniqueName: mVertex.getProperty("uniqueName"),
                              displayText: mVertex.getProperty("highlightedText"),
                              type: mVertex.getProperty("type"),
                              subType: mVertex.getProperty("subType"),
                              date: mVertex.getProperty("modificationDate"),
                              dateAdded: mVertex.getProperty("creationDate"),
                              publication: publication,
                              relativeFilePath: file,
                              page: mVertex.getProperty("page"),
                              bibtexKey: bibtexKey
                          ])
                          break;
                    }
                    break;
            }
        }
        def result = JsonOutput.toJson(json)
        logger.warn("Result of the query: {}",result)

        long finish = System.currentTimeMillis()
        logger.warn("constructResultJson method finished in {} seconds",(finish-start)/1000)

        return result

      }

}
