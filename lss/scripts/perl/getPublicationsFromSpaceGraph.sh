#!/bin/sh

export JAVA_HOME=/usr/lib/jvm/jdk1.8.0_74
export GROOVY_HOME=/opt/groovy-2.4.6
export PATH=$GROOVY_HOME/bin:$PATH

groovy -cp scripts/groovy scripts/groovy/getPublicationsFromSpaceGraph.groovy

exit 0
