# Database integrity check

## Check that all files are valid

There is a stronger check - opening all the files with the pdf browser, but it would take a few ages...

* Select each entry in the Zotero database
* Check if it has file attachments
* The file exists on disk
