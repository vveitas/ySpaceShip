# Check database synchronization

* The Zotero library exists and accessible remotely
* The connection to pbrain database is open

## Databases synchronized

* Get the timestamp of Zotero database
* Get the timestamp of PBrain database
* The timestamps are equal

## Databases unsynchronized

* Get the timestamp of Zotero database
* Get the timestamp of PBrain database
* The timestamps are not equal

## Database synchronization

* Every entry is synchronized
* The database is synchronized

## Entry synchronized

* Given an entry in the Zotero
* The entry's timestamp in zotero is equal to timestamp on pbrain;
* The the timestamp of Zotero attachment is equal to the timestamp of PDF file;

## Entry is newer in Zotero than in Pbrain

* Given an entry in the Zotero
* The entry's timestamp in Zotero is newer than in Pbrain;
* Get the matadata of Zotero entry
* Record entry to the PBrain Database

## Document is newer than Pbrain entry

* Given an entry in the Zotero database
* The timestamp of the attached PDF file is newer in document library than PBrain database
* Get the PDF document's annotations (pbrain-pdf module)
* Record document's annotations to Pbrain Database



