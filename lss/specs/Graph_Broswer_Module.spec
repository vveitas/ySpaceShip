# Graph Browser Module

The context of the graph exploration module describes the prerequisites for the scenarios to work:

* A graph is connected to the exploration module;
* The graph is not empty
* Graph traversal query is defined;

## Vertex - based exploration

* Select a vertex in a graph;
* Select the radius of exploartion;
* The resulting subgraph is displayed in "GraphView";
