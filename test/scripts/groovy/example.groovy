@Grab(group="org.seleniumhq.selenium", module="selenium-java", version="2.42.2")

import org.openqa.selenium.*;
import org.openqa.selenium.chrome.*;

System.setProperty("webdriver.chrome.driver", "./test/webdrivers/chromedriver");

ChromeOptions chromeOptions = new ChromeOptions();
chromeOptions.addArguments("--verbose", "--ignore-certificate-errors");
WebDriver driver = new ChromeDriver();
driver.get("http://auto.ria.com/?target=search&event=big&city=&state=5&category_id=1&marka=75&model=663&currency=1&page=0");
List<WebElement> advs = driver.findElements(By.cssSelector("div.content-bar"));
for(WebElement e : advs) {
    try {
        WebElement header = e.findElement(By.cssSelector("h3 > a"))
        println header.getText();
   } catch(all) { }
}
driver.close();
driver.quit();
