#!/bin/sh
#script is called from $PROJECT_HOME/test directory

DEP_DIR="deployment"
ABS_PATH=$(pwd)

sudo rm -r "$DEP_DIR"
mkdir -p "$DEP_DIR"

echo "$ABS_PATH/pbrain-library" > "$DEP_DIR/pdflibrary.conf"

cp ../scripts/yspaceship.sh $DEP_DIR
