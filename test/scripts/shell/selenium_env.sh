#!/bin/bash

cd test
if [ ! -d "webdrivers" ]; then mkdir -p webdrivers; fi;
cd webdrivers;
if [ ! -f "chromedriver" ]; then 
	wget "http://chromedriver.storage.googleapis.com/LATEST_RELEASE";
	read LATEST_CHROME<LATEST_RELEASE;
	wget "http://chromedriver.storage.googleapis.com/$LATEST_CHROME/chromedriver_linux64.zip"
	unzip chromedriver_linux64.zip;
	rm chromedriver_linux64.zip;
	rm LATEST_RELEASE
fi;