# Dan Sosedoff
*https://sosedoff.com/2013/12/17/cleanup-docker-containers-and-images.html*

## Cleanup after build

Typically i build images with the following command:

```
docker build -t=myimage -rm=true .
```

This will build a new image with name myimage and after build is complete Docker will automatically remove intermediate containers
