#!/bin/bash

export JAVA_HOME=/opt/yspaceship/required/jre
export GROOVY_HOME=/opt/yspaceship/required/groovy
export PATH=$GROOVY_HOME/bin:$PATH

if [ ! -d lss/scripts/java ]; then mkdir -p lss/scripts/java; else rm lss/scripts/java/*; fi;
cp lss/scripts/groovy/* lss/scripts/java/
cd lss/scripts/java

#remove all @Grab annotations from the file -- cannot be run by java
sed -i '/@Grab/d' *.groovy

#compile
groovyc -cp "/opt/yspaceship/required/groovy/grapes/*:/opt/yspaceship/required/groovy/lib/*:/opt/yspaceship/required/titan-server-0.4.4/lib/*" -encoding utf-8 --indy *.groovy
rm *.groovy
